#!/usr/bin/env python

i = 1
for x in xrange(1, 13):
    row = [str(y * i) for y in xrange(1, 13)]
    print "".join(n.rjust(4) for n in row)
    i += 1
