#!/usr/bin/env python
"""
A number is a self-describing number when (assuming digit positions are labeled 0 to N-1), the digit in each position is equal to the number of times that that digit appears in the number.
"""
import sys

def is_self_describing(n):
    for i in range(len(n)):
        # position i has value n[i]
        # are there n.count(i) occurrences of n[i] in n?
        if n.count(str(i)) != int(n[i]):
            return 0
    return 1

data = open(sys.argv[1])
for line in data:
    n = line.rstrip('\n')
    result = is_self_describing(n)
    print(result)
