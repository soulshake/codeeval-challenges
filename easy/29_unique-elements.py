#!/usr/bin/env python
import sys

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        data = f.read().split("\n")
        data.remove('')
    for item in data:
        item = list(set(item.split(',')))
        item.sort()
        print(",".join(item))
