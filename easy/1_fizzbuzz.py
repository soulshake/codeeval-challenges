#!/usr/bin/env python
"""
fb = __import__('1_fizzbuzz')
%timeit fb.fizzbuzz('1.input')
10000 loops, best of 3: 57.1 us per loop  <-- not this version
"""
import sys

def make_row(x, y, n):
    results = ''
    for num in range(1, n + 1):
        result = "F" if not num%x else ""
        result += "B" if num%y == 0 else ""
        results += str(num) + " " if not result else result + " "
    return results[:-1]

def fizzbuzz(filename):
    with open(filename, 'r') as f:
        return "\n".join([
            make_row(*[int(item) for item in line.split()])
            for line in f.read().split('\n') if line])

if __name__ == '__main__':
    result = fizzbuzz(sys.argv[1])
    print(result)
