#!/usr/bin/env python3
import sys

for line in open(sys.argv[1].rstrip('\n')):
    line = [int(x) for x in line.split()]
    unique = [x for x in line if line.count(x) == 1]
    winner = line.index(min(unique)) + 1 if unique else 0
    print(winner)
