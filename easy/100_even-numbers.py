#!/usr/bin/env python
import sys

data = open(sys.argv[1])
for line in data:
    n = int(line.rstrip('\n'))
    result = 1 if n%2 == 0 else 0
    print(result)
