#!/usr/bin/env python
import sys

for line in open(sys.argv[1], "r"):
    line = line.strip('\n')
    count = 0
    for i in range(len(line)):
        l = line[i:]
        if l.startswith('>>-->') or l.startswith('<--<<'):
            count += 1
    print(count)
