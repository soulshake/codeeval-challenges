/* https://www.codeeval.com/open_challenges/4/
SUM OF PRIMES

CHALLENGE DESCRIPTION:
Write a program which determines the sum of the first 1000 prime numbers.

INPUT SAMPLE:
There is no input for this program.

OUTPUT SAMPLE:
Print to stdout the sum of the first 1000 prime numbers.
> 3682913
*/

package main

import "fmt"
import "math"

func main() {
	// Make a slice of the first 1000 prime numbers
	primes := make([]float64, 0)
	primes = append(primes, 2) // Don't forget the only even one
	for num := 3.0; len(primes) < 1000; num = num + 2 {
		if isPrime(num) {
			primes = append(primes, num)
		}
	}
	fmt.Printf("%.f", sum(primes))
}

func sum(s []float64) float64 {
	var total float64
	for _, v := range s {
		total += v
	}
	return total
}

func factor(num float64) []float64 {
	divisors := make([]float64, 0)
	for divisor := 1.0; divisor <= num/2; divisor++ {
		if math.Remainder(num, divisor) == 0 {
			divisors = append(divisors, num)
		}
	}
	return divisors
}

func isPrime(num float64) bool {
	divisors := factor(num)
	if len(divisors) <= 1 {
		// We have a prime number
		return true
	}
	return false
}
