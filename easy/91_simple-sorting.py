#!/usr/bin/env python
import sys

if __name__ == "__main__":
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')
        if '' in data:
            data.remove('')
    for line in data:
        line = [float(number) for number in line.split()]
        line.sort()
        print(" ".join(["{:.3f}".format(number) for number in line]))
