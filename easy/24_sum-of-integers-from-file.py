#!/usr/bin/env python
import sys

data = open(sys.argv[1])
i = 0
for line in data:
    i += int(line.rstrip('\n'))
print(i)
