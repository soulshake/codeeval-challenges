#!/usr/bin/env python
import sys

for line in open(sys.argv[1]):
    line = line.rstrip('\n')
    x, n = line.split(',')
    x, n = int(x), int(n)

    i = 2
    while True:
        result = n * i
        if n * i >= x:
            print(result)
            break
        else:
            i += 1
