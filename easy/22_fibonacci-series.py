#!/usr/bin/env python
import sys


def fibonacci(n):
    if n == 0:
        return 0
    result = [1, 1]
    for x in range(n):
        if len(result) >= n:
            return result[:n][-1]
        result.append(result[-1] + result[-2])
    return result[-1]

for line in open(sys.argv[1]):
    line = int(line.rstrip('\n'))
    print(fibonacci(line))
