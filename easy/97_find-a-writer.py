#!/usr/bin/env python
import sys

for line in open(sys.argv[1]):
    line = line.strip('\n').split('|')
    if line == ['']:
        continue
    a, b = line[0], [int(x) for x in line[1].split()]
    a = "x" + a
    result = "".join([a[i] for i in b])
    print(result)
