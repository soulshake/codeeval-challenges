#!/usr/bin/env python
import sys

# https://www.codeeval.com/open_challenges/20/

filename = sys.argv[1]

with open(filename, 'r') as f:
    print(f.read().lower())
