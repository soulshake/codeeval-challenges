#!/usr/bin/env python
import sys

for line in open(sys.argv[1]):
    line = line.rstrip('\n')
    n, p1, p2 = line.split(',')
    print(n, p1, p2)
