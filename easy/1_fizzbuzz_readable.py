#!/usr/bin/env python
import sys

def fizzbuzz(filename):
    results = []
    with open(filename, 'r') as f:
        lines = f.read().split('\n')
    if '' in lines:
        lines.remove('')

    # constraints
    for line in lines:
        x, y, n = [int(item) for item in line.split()]

        result = ''
        for num in range(1, n + 1):
            if num%x == 0:
                result += 'F'
            if num%y == 0:
                result += 'B'
            if num%y != 0 and num%x != 0:
                result += str(num)
            result += ' '

        result = result[:-1]
        results.append(result)
    return "\n".join(results)

if __name__ == '__main__':
    results = fizzbuzz(sys.argv[1])
    print(results)
