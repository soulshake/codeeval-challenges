#!/usr/bin/env ruby

File.open(ARGV[0]).each_line do |line|
  line.strip!
  line = line.split(',')
  n = line[0]
  p1 = line[1].to_i
  p2 = line[2].to_i
  if n[p1] == n[p2]
    puts true
  else
    puts false
  end
end
