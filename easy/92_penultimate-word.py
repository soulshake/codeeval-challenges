#!/usr/bin/env python
# https://www.codeeval.com/open_challenges/92/
import sys

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')
        data.remove('')
    for line in data:
        print(line.split()[-2])

