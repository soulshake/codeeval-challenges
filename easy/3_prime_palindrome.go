/* https://www.codeeval.com/open_challenges/3/
Write a program which determines the largest prime palindrome less than 1000.

INPUT SAMPLE:
There is no input for this program.

OUTPUT SAMPLE:
Print to stdout the largest prime palindrome less than 1000.
> 929

*/

package main

import "fmt"
import "math"
import "os"

func main() {
	// We want the largest prime palindrome of less than 1000,
	// so we count backwards by 2 from 999
	for num := 999.0; num > 0; num = num - 2 {
		divisors := make([]float64, 0)
		for divisor := 1.0; divisor <= num/2; divisor++ {
			if math.Remainder(num, divisor) == 0 {
				divisors = append(divisors, divisor)
			}
		}
		if len(divisors) <= 1 {
			// We have a prime number!
			// Now we need to see if it's a palindrome.
			rev := reverse(num)
			// If it's the same backwards and forwards, we have a palindrome
			if num == rev {
				fmt.Println(num)
				os.Exit(0)
			}
		}
	}
}

func reverse(n float64) float64 {
	// Reverse a number, e.g. 12345 -> 54321
	rev := 0.0
	for num := n; num > 0; {
		digit := math.Mod(num, 10.0) // num modulus 10, AKA the last digit
		rev = (rev * 10) + digit     // we multiply it by 10
		num = (num - digit) / 10
	}
	return rev
}
