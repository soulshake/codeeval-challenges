package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	file, _ := os.Open(os.Args[1])
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		y := scanner.Text()
		line := strings.Split(y, " ")
		x := make([]string, len(line))
		for i, word := range line {
			x[len(line)-i-1] = word
		}

		fmt.Println(strings.Join(x, " "))
	}
}
