#!/usr/bin/python3
"""
num = 979
n = num
rev = 0
while (num > 0):
    dig = num%10
    rev = rev * 10 + dig
    num = num / 10
if num == rev:
    return num

"""


def possible_factors(start_with=2, end_with=1000):
    # Generates 2, 3, 5, 7, 9... starting at "start_with"
    yield start_with
    if start_with == 2:
        next = 3
    else:
        next = start_with + 2
    while next <= end_with:
        yield next
        next += 2

def factors(n, last_factor=2):
    if n < 4:
        return [n]
    for f in possible_factors(last_factor):
        if n == f:
            return [f]
        if n%f == 0:
            return [f] + factors(n/f, f)
    return [n]

def reverse(num):
    n = num
    rev = 0
    while num > 0:
        dig = num%10
        num -= dig
        rev = rev * 10 + dig
        num = num / 10
        print(num)
    return rev

if __name__ ==  '__main__':
    #for i in possible_factors(1, 10, 2):
    print(reverse(12345))
    """
    for i in range(1, 1000):
        print("{} <> {}".format(i, reverse(i)))

    ret = max([factors(i)[0] for i in range(1, 1000, 2)
        #if len(factors(i)) == 1 and str(factors(i)[0])[::-1] == str(factors(i)[0])])
        if len(factors(i)) == 1 and reverse(factors(i)[0]) == factors(i)[0]])
    print(ret)
    """
