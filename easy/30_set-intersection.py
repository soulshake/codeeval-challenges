#!/usr/bin/env python
import sys

for line in open(sys.argv[1]):
    line = line.rstrip('\n')
    s1, s2 = line.split(';')
    s1 = set(s1.split(','))
    s2 = set(s2.split(','))
    intersection = list(s2.intersection(s1))
    intersection.sort()
    print(",".join(intersection))
