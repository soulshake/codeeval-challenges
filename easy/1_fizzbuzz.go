package main

// Note: Should use ints and modulo instead of floats and remainder

import "fmt"
import "log"
import "bufio"
import "os"
import "strings"
import "strconv"
import "math"

func main() {
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := strings.Split(scanner.Text(), " ")
		x, _ := strconv.ParseFloat(line[0], 10)
		y, _ := strconv.ParseFloat(line[1], 10)
		n, _ := strconv.ParseFloat(line[2], 10)
		results := ""
		for i := 1.0; i < n+1; i++ {
			result := ""

			if math.Remainder(i, x) == 0 {
				result += "F"
			}

			if math.Remainder(i, y) == 0 {
				result += "B"
			}

			if result == "" {
				result = strconv.FormatFloat(i, 'f', 0, 64)
			}
			results += result + " "
		}
		results = strings.Trim(results, " ")
		fmt.Println(results)
	}
}
