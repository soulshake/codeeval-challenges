/*
You have 2 lists of positive integers. Write a program which multiplies corresponding elements in these lists.

INPUT SAMPLE:

Your program should accept as its first argument a path to a filename. Input example is the following

9 0 6 | 15 14 9
5 | 8
13 4 15 1 15 5 | 1 4 15 14 8 2

The lists are separated with a pipe char, numbers are separated with a space char. 
The number of elements in lists are in range [1, 10]. 
The number of elements is the same in both lists. 
Each element is a number in range [0, 99].
*/

package main

import "fmt"
import "os"
import "bufio"
import "strings"
import "log"
import "strconv"


func main() {
    file, err := os.Open(os.Args[1])
    if err != nil {
        log.Fatal(err)
    }

    defer file.Close()
    scanner := bufio.NewScanner(file)
    var a []string
    var b []string
    for scanner.Scan() {
        line := strings.Split(scanner.Text(), " | ")
        if len(line) == 1 {
            a = append(a, line[0])
            b = append(b, line[1])
        } else {
            a = strings.Split(line[0], " ")
            b = strings.Split(line[1], " ")
        }
        result := make([]string, 0)
        for i := 0; i < len(a); i++ {
            A, _ := strconv.ParseFloat(a[i], 10)
            B, _ := strconv.ParseFloat(b[i], 10)
            C := strconv.FormatFloat(A * B, 'f', 0, 64)

            result = append(result, C)
        }
        toprint := strings.Join(result, " ")

        fmt.Println(toprint)
    }

}

