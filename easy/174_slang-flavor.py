#!/usr/bin/env python
import sys

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')

    slang = [
        ", yeah!",
        ", this is crazy, I tell ya.",
        ", can U believe this?",
        ", eh?",
        ", aw yea.",
        ", yo.",
        "? No way!",
        ". Awesome!",
        ]
    slang.reverse()

    i = 1
    for line in data:
        if not line:
            continue
        result = ''
        for letter in line:
            if letter in ['.', '!', '?']:
                if i%2 == 0:
                    word = slang.pop()
                    result += word
                    slang.insert(0, word)
                else:
                    result += letter
                i += 1
            else:
                result += letter
        print(result)
