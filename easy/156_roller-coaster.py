#!/usr/bin/env python
import sys

with open(sys.argv[1]) as f:
    data = f.read().split('\n')

for line in data:
    result = []
    b = True  # set a boolean we can toggle
    for char in line:
        if char.isalpha():
            case = [char.lower(), char.upper()]
            result.append(case[int(b)])
            b = not b
        else:
            result.append(char)
    print("".join(result))
        
