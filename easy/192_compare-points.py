#!/usr/bin/env python
import sys

for line in open(sys.argv[1]):
    line = line.strip('\n')
    line = [int(x) for x in line.split()]
    O, P, Q, R = line
    here = (O, P)
    there = (Q, R)
    result = 'here' if here == there else ''
    if there[1] > here[1]:
        result += 'N'
    if there[1] < here[1]:
        result += 'S'
    if there[0] > here[0]:
        result += 'E'
    if there[0] < here[0]:
        result += 'W'
    print(result)
