#!/usr/bin/env python
import sys

for line in open(sys.argv[1]):
    line = line.strip('\n').split()
    maxlen = max([len(word) for word in line])
    maxword = [word for word in line if len(word) == maxlen]
    print(maxword[0])
