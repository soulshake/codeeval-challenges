#!/usr/bin/env python
import sys

def square_digits(n):
    return sum([int(digit)**2 for digit in str(n)])


def is_happy(n):
    """ Starting with any positive integer, replace the number by the sum of the squares of its digits, and repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1. Those numbers for which this process ends in 1 are happy numbers, while those that do not end in 1 are unhappy numbers."""
    unhappy = set()  # keep track of results to avoid loops
    while True:
        n = square_digits(n)
        if n == 1:
            return n
        if n in unhappy:
            return 0
        unhappy.add(n)
    return 0

data = open(sys.argv[1])
for line in data:
    n = int(line.rstrip('\n'))
    print(is_happy(n))

