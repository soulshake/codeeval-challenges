#!/usr/bin/env python
import sys

for line in open(sys.argv[1]):
    line = line.strip('\n').split(',')
    digits = ",".join([x for x in line if x.isdigit()])
    words = ",".join([x for x in line if x.isalpha()])
    print("|".join([words, digits]).strip('|'))
