#!/usr/bin/env python
import sys

for line in open(sys.argv[1]):
    line = int(line.rstrip('\n'))
    print(sum([int(x) for x in str(line)]))
