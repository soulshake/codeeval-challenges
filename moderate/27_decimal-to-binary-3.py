#!/usr/bin/env python3
import sys

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        for line in f:
            line = line.rstrip('\n')
            binary = bin(int(line))
            print(binary[2:])
