#!/usr/bin/env python
import sys

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')

    for line in data:
        if not line:
            continue
        for letter in line:
            if line.count(letter) == 1:
                print(letter)
                break
