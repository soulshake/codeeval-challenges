#!/usr/bin/env python
import sys

if __name__ == "__main__":
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')

    data = [line.split() for line in data if line]
    print("\n".join([line[-int(line[-1])-1] for line in data if int(line[-1]) < len(line)]))
        
