#!/usr/bin/env python
import sys

if __name__ == "__main__":
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')

    print("\n".join([line.swapcase() for line in data if line]))
