#!/usr/bin/env python
import sys

if __name__ == "__main__":
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')

    data = [line.split(',') for line in data if line]
    print("\n".join([str(line[0].rfind(line[1])) for line in data]))
