#!/usr/bin/env python
import sys

def find_highest_subarray(a):
    highest = None
    for start in range(len(a)):
        length = len(a) - start + 1
        for l in range(1, length):
            result = a[start:start+l]
            if not highest:
                highest = a[start:start+l]
            if sum(result) > sum(highest):
                highest = a[start:start+l]
                
    return highest

for line in open(sys.argv[1]):
    line = line.rstrip('\n').split(',')
    line = [int(x) for x in line]
    print(sum(find_highest_subarray(line)))
