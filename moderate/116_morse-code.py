#!/usr/bin/env python
import sys

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')

    code = {}
    code[".-"] = "a"
    code["-..."] = "b"
    code["-.-."] = "c"
    code["-.."] = "d"
    code["."] = "e"
    code["..-."] = "f"
    code["--."] = "g"
    code["...."] = "h"
    code[".."] = "i"
    code[".---"] = "j"
    code["-.-"] = "k"
    code[".-.."] = "l"
    code["--"] = "m"
    code["-."] = "n"
    code["---"] = "o"
    code[".--."] = "p"
    code["--.-"] = "q"
    code[".-."] = "r"
    code["..."] = "s"
    code["-"] = "t"
    code["..-"] = "u"
    code["...-"] = "v"
    code[".--"] = "w"
    code["-..-"] = "x"
    code["-.--"] = "y"
    code["--.."] = "z"
    code["-----"] = "0"
    code[".----"] = "1"
    code["..---"] = "2"
    code["...--"] = "3"
    code["....-"] = "4"
    code["....."] = "5"
    code["-...."] = "6"
    code["--..."] = "7"
    code["---.."] = "8"
    code["----."] = "9"
    code[""] = ""

    for l in data:
        if not l:
            continue
        line = []
        for w in l.split('  '):
            word = ''
            for letter in w.split(' '):
                word += code[letter]
            line.append(word.upper())
        print(" ".join(line))
