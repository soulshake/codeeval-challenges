#!/usr/bin/env python
import sys

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')
    if '' in data:
        data.remove('')
    for line in data:
        item, chars = line.split(', ')
        for letter in chars:
            while letter in item:
                item = item.replace(letter, '')
        print(item)

