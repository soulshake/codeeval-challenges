#!/usr/bin/env python
import sys

def is_subloop(a, b):
    # Return True if b appears in a
    if len(a) < len(b):
        return False
    if a == b:
        return True
    if len(b) == 0:
        return False
    # Make sure everything in the subloop appears at least once in the loop
    if 0 in [ a.count(x) for x in b ]:
        return False
    # alternate way: 
    if set(b).isdisjoint(set(a)):
        return False

    start = b[0]
    bi = 0
    if a.count(start) > 0:
        ai = a.index(start)
        for item in b:
            if b[bi] != a[ai]:
                return False
            ai += 1
            bi += 1
        else:
            print("{} is a subloop of {}".format(b, a))
            return True
    return False

def remove_subloop(a, b):
    a.reverse()
    b.reverse()
    i = a.index(b[0])
    print('---')
    print("Removing {} from {}".format(b, a))
    for x in range(len(b)):
        print(a)
        a.pop()
    a.reverse()
    return a

def loop_repeats(a, b):
    a = remove_subloop(a, b)
    if a[:len(b)] == b: #is_subloop(a, b):
        print("{} appears more than once in {}!".format(b, a))
        return b
    return False

for line in open(sys.argv[1]):
    line = line.rstrip('\n').split()
    line = [int(x) for x in line]
    line.reverse()
    to_check = []
    for i in range(1, len(line)):
        length = len(line)# - i
        if length > len(line)/2:
            continue
        to_check.append(line[:length])
    for subloop in to_check:
        is_loop = is_subloop(line, subloop)
        if is_loop and loop_repeats(line, subloop):
            print(subloop)
            break
        #for l in range(1, length):
        #    if l <= i:
        #        continue
        #    print(line[length:-1])
        #    length = length - 1
    exit()
    for item in line:
        print(item)
    exit()
    result = is_subloop(line, [11, 9, 7, 2, 5, 6])
    #result = is_subloop(line, ['a', 'b', 'c'])
    print(result)
