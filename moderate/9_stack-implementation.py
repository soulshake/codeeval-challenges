#!/usr/bin/env python
import sys

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        data = [x.split() for x in f.read().split('\n') if x]
    for line in data:
        result = []
        while line:
            result.append(line.pop())
            if line:
                line.pop()
        print(" ".join(result))
