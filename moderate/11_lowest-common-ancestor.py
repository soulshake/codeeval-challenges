#!/usr/bin/env python
import sys

def node_to_path(tree, node, path=[]):
    # this is a binary search tree, not just a binary tree, so i should compare sizes instead of checking both sides
    # i need a result of size p (p being the length of the path), so if my complexity is p, i know i can't go faster
    while node not in path:
        if not tree:
            return path
        if node in tree.keys(): # we found the node we're looking for
            path.append(node)
            return path
        else:
            for key in tree:
                if tree[key] is None:  # we've reached a dead end without finding our node
                    continue
                this_path = node_to_path(tree[key], node, path)
                if node in this_path:
                    path.insert(0, key)
    return path

def find_last_common_element(a, b):
    last_match = None
    for pair in zip(a, b):
        if pair[0] == pair[1]:
            last_match = pair[0]
        else:
            return last_match

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        # Not ideal for big input
        # Better: for line in open(sys.argv[1]):
        #            pair = line.split()
        data = [x.split() for x in f.read().split('\n') if x]
 
    tree = { 30: { 52: None, 8: { 3: None, 20: { 10: None, 29: None } } } }
    # since tree is hardcoded, a more efficient way is possible, e.g.:
    # for each element, store its ancestor
    # for root, set its ancestor to None. Then when you find the element, just return ancestors all the way back to the root

    for pair in data:
        paths = []
        pair = [int(x) for x in pair]
        for n in pair:
            path = node_to_path(tree, n, path=[])
            paths.append(path)

        lca = find_last_common_element(paths[0], paths[1])
        print(lca)
