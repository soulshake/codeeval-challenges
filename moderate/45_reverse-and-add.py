#!/usr/bin/env python
import sys

def is_palindrome(n):
    return str(n) == rev(n)

def rev(n):
    x = [x for x in str(n)]
    x.reverse()
    return "".join(x)

if __name__ == "__main__":
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')

    data = [int(line) for line in data if line]
    for n in data:
        i = 0
        while not is_palindrome(n):
            n += int(rev(n))
            i += 1
            if i > 1000:
                break
        print("{} {}".format(i, n))
