#!/usr/bin/env python
import sys

if __name__ == "__main__":
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')

    data = [line.split(';') for line in data if line]
    for length, numbers in data:
        numbers = numbers.split(',')
        result = [n for n in numbers if numbers.count(n) > 1]
        print(result[0])

