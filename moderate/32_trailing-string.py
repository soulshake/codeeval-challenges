#!/usr/bin/env python
import sys

# https://www.codeeval.com/open_challenges/32/

if __name__ == '__main__':
    with open(sys.argv[1], 'r') as f:
        data = f.read().split('\n')
    for line in data:
        pair = line.split(',')
        if len(pair) == 2:
            if pair[0].endswith(pair[1]):
                print(1)
            else:
                print(0)
