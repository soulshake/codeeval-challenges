package main

import "fmt"
import "log"
import "bufio"
import "os"
import "strconv"
import "strings"
import "sort"

func main() {
	// open the input file
	file, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	// scan the first line of the file, which will contain the number of results we should return
	scanner.Scan()
	count, _ := strconv.Atoi(scanner.Text()) // convert from string to int

	// read the rest of the lines from the file into a slice
	lines := make([]string, 0)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	// sort it by length, reverse it, and print the first N results
	sort.Sort(sort.Reverse(ByLength(lines)))
	fmt.Println(strings.Join(lines[:count], "\n"))
}

// In order to sort by a custom function in Go, we need a
// corresponding type. Here we create a `ByLength` type
// that is just an alias for the builtin `[]string` type.
type ByLength []string

// We implement `sort.Interface` - `Len`, `Less`, and
// `Swap` - on our type so we can use the `sort` package's
// generic `Sort` function.
func (s ByLength) Len() int {
	return len(s)
}
func (s ByLength) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s ByLength) Less(i, j int) bool {
	return len(s[i]) < len(s[j])
}
