#!/usr/bin/env python
import sys

def make_paths(height):
    # Calculate all possible paths in a balanced binary tree
    # Count to x (where x is the total number of possible paths) and return a
    # list of binary strings (where 0=L, 1=R)

    # Number of possible paths = height of the tree (minus the root), squared
    paths_count = 2**(height-1)

    # Make a list of as many sequential binary strings as there are possible paths
    combos = []
    for p in xrange(paths_count):
        binary = bin(p)[2:]
        combos.append(binary.zfill(height-1))
    #for c in combos:
    #    print(c)
    return combos

def print_tree(data):
    for row in data:
        print("{}{}".format(" "*(len(data[-1]) - len(row)), row))

def max_path_sum(data):
    # accepts a list of lists, where each item is a list containing one more integer than the one before
    #print_tree(data)
    combos = make_paths(len(data))
    print("There are {} possible paths.".format(len(combos)))
    paths_count = len(combos)
    paths = {}
    root = data[0][0]

    # for each series of steps (LRLR, etc), calculate path and add it to the paths dict
    for combo in combos:
        paths[combo], position = [root], 0
        row_num = 0
        for row, step in zip(data[1:], [int(x) for x in combo]):
            position += step  # column remains the same or increases by 1
            paths[combo].append(row[position])
            row_num += 1

    # Sum all the paths, and return the highest result
    path_sum = max([sum(path) for path in paths.values()])
    return path_sum

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        data = [[int(y) for y in x.split()] for x in f.read().split('\n') if x]
        print(max_path_sum(data))
