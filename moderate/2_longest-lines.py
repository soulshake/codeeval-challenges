#!/usr/bin/env python
import sys

def get_items_by_length(items, length):
    """Return the longest string or strings in a set."""
    return [x for x in data if length == len(x)]

if __name__ == '__main__':

    with open(sys.argv[1], 'r') as f:
        data = f.read().split('\n')
    count = int(data.pop(0))
    if not count:
        exit()  # Just exit now if we're being asked to print 0 lines
    results = []

    for position in range(count):
        maxlen = max([len(x) for x in data])
        longest = get_items_by_length(data, maxlen)
        results.extend(longest)
        for x in longest:
            data.remove(x)
        if not data:
            break  # Don't panic if we're asked to print more lines than we have
    print("\n".join(results[:count]))

    # naive implementation: sort by length

    # range(0, count) is the same thing as range(count)
    # complexity of my method is n times the number of lines desired ("c").
    # complexity of sorting with quick sort followed by printing first c lines would 
    # be n log n.
    # my method's complexity: n * c
    # so: n * c vs. n * log n
    # sorting is thus more efficient only if c is more than log*n

