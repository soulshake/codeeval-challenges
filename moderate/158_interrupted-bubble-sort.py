#!/usr/bin/env python
import sys

def sort_step(l):
    l = [x for x in l]
    for i in range(len(l) - 1):
        a, b = l[i], l[i+1]
        if b < a:
            l[i], l[i+1] = l[i+1], l[i]
    return l

def print_list(l):
    return " ".join([str(x) for x in l])

for line in open(sys.argv[1]):
    line = line.strip('\n')
    line, iterations = line.split('|')
    line = [int(x) for x in line.split()]

    i = 0
    iterations = int(iterations)
    while i < iterations:
        next_step = sort_step(line)
        if next_step == line:
            break
        line = next_step
        i += 1
    print(print_list(line))
