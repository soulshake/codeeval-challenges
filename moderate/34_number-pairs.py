#!/usr/bin/env python

import sys

for line in open(sys.argv[1]):
    line = line.strip('\n')
    array, target = line.split(';')
    array = [int(x) for x in array.split(',')]
    pool = set()
    for start in range(len(array)):
        a = array[start]
        for i in range(start, len(array)):
            b = array[i]
            c = a+b
            if a == b and array.count(a) == 1:
                continue
            pool.add((a, b))
    pool = sorted(list(pool))
    result = ";".join([",".join((str(a),str(b))) for a,b in pool if a+b == int(target)])
    print(result if result else "NULL")
