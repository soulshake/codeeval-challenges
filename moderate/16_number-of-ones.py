#!/usr/bin/env python
import sys

if __name__ == "__main__":

    with open(sys.argv[1]) as f:
        for line in f:
            line = line.rstrip('\n')
            binary = bin(int(line))[2:]
            print(binary.count('1'))  # binary is a string, no need to convert

    exit()
    #data = f.read().split('\n')
    data = [line for line in data if line]
    data = [bin(int(x))[2:] for x in data]
    print("\n".join([str(x.count('1')) for x in data]))

    #######################


