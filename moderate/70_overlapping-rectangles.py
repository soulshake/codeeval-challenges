#!/usr/bin/env python
import sys

def print_grid(grid):
    header = sorted(grid.keys())
    for x in grid:
        y_labels = sorted(grid[x].keys())
        break

    y_labels.reverse()
    rows = [[""] + header]
    for y in y_labels:
        row = [y]
        for x in header:
            row.append(grid[x][y])
        rows.append(row)
    for row in rows:
        print("".join([str(item).rjust(4) for item in row]))

def overlaps(ax1, ay1, ax2, ay2, bx1, by1, bx2, by2):
    if ax1 < bx1 and ax2 < bx1:
        return False
    if ax1 > bx2 and ax2 > bx2:
        return False
    if ay1 > by1 and ay2 > by1:
        return False
    if ay1 < by2 and ay2 < by2:
        return False
    return True

for line in open(sys.argv[1]):
    line = line.rstrip('\n').split(',')
    line = [int(x) for x in line]
    print('---')
    print(overlaps(*line))

    a = line[:4]
    b = line[4:]
    a = [(a[0], a[1]), (a[2], a[3]), (a[0], a[3]), (a[2], a[1])]
    b = [(b[0], b[1]), (b[2], b[3]), (b[0], b[3]), (b[2], b[1])]

    X = [line[0], line[2], line[4], line[6]]
    Y = [line[1], line[3], line[5], line[7]]
    grid = {}
    x_range = [min(X), max(X)]
    y_range = [min(Y), max(Y)]
    x_range[-1] += 1
    y_range[-1] += 1

    for x in range(*x_range):
        grid[x] = {}
        for y in range(*y_range):
            grid[x][y] = '-'

    label = 'a'
    for item in [a, b]:  # so far so good
        for x in grid:
            for y in grid[x]:
                if (x,y) in item:
                    grid[x][y] = label.upper()
        label = 'b'
    print_grid(grid)
