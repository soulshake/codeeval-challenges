#!/usr/bin/env python
#https://www.codeeval.com/open_challenges/28/
import sys

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')
        data.remove('')
    for pair in data:
        pair = pair.split(',')
        one, two = pair[0], pair[1]
        ii = 0
        print(one, two)
        if two in one:
            print('true')
            continue
        for i in range(len(one)):
            print(one[i], two[ii])
            if one[i] == two[ii]:
                ii = ii + 1
            elif two[ii] == '*' and two[ii-1] != '\\':
                pass
            else:
                print('false')
                break
            if i+1 == len(one):
                print('true')
