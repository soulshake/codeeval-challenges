#!/usr/bin/env python
import sys
# This only "partially" works. Maybe bitwise operations?

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')

    for line in data:
        if not line:
            continue
        item, replacements = line.split(';', 1)
        result = item
        check = item

        l = replacements.split(',')
        i = 0
        sources, targets = [], []
        for x in l:
            if i%2 == 0:
                sources.append(x)
            else:
                targets.append(x)
            i += 1

        for source, target in zip(sources, targets):
            while source in check and source in item:
                i = check.index(source)
                if result[i:i+len(source)] == item[i:i+len(source)]:
                    result = list(result)
                    result[i:i+len(source)] = target # + ' ' * (len(source) - len(target))
                    result = "".join(result)

                check = list(check)
                check[i:i+len(source)] = 'x' * len(source)
                check = "".join(check)

        print(result)
