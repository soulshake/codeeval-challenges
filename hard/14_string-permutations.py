#!/usr/bin/env python
# https://www.codeeval.com/open_challenges/14/
import sys
import random
import math

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')
    data.remove('')
    for word in data:
        word = list(word)
        perms = set()
        num_perms = math.factorial(len(word))
        while len(perms) < num_perms:
            random.shuffle(word)
            perms.add("".join(word))
        perms = list(perms)
        perms.sort()
        print(",".join(perms))
