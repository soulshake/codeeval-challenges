#!/usr/bin/env python
import sys
import re

def title_only_first_letter(s):
    return s[0].upper() + s[1:]

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        data = f.read().split('\n')
    if '' in data:
        data.remove('')
    for line in data:
        result = [title_only_first_letter(word) for word in line.split()]
        print(" ".join(result))
